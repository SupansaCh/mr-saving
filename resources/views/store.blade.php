@extends('layouts.app')

@section('content')

    <div class="container p-3">
            <form class="form-inline">
                <input class="form-control mr-sm-2" type="search" placeholder="ร้านอาหารที่บันทึกไว้" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">ค้นหาร้าน</button>
            </form>
        <p class="container"></p>
        <p class="p-2" style="background-color:#C1DF5B;">ร้านอาหารทั้งหมด</p>
        <div class="row">

                @if(count($img_uploads)>0)
                    @foreach($img_uploads as $img_upload)
                    <div class="col-4">
                <div class="card" style="width: 20rem; height: auto;">
                    <img style="width: 20rem; height: 20rem" src="{{url('uploads/'.$img_upload->img)}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$img_upload->store}}</h5>
                        <p class="card-text" style="color: #aaaaaa;">ตำแหน่งที่ตั้ง : {{$img_upload->place}}</p>
                        <p class="card-text" style="color: #38c172;">เปิดเวลา : {{$img_upload->timeOpen}}</p>
                        <p class="card-title" style="color:#e3342f;">ปิดเวลา : {{$img_upload->timeClose}}</p>

                        <a href="#" class="btn btn-outline-secondary btn-block" style=" border-radius: 20px;">เลือก</a>
                        <button class="btn btn-outline-primary btn-block" style=" border-radius: 20px;"><a href="{{url('uploads/'.$img_upload->id)}}">แก้ไข</a></button>
                        <div class="col mt-2" style="margin-left: 80px;">
                            <form action="{{url('uploads/'.$img_upload->id)}}" method="post" id="form-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-outline-danger" onclick="confirm_delete()" type="button">Delete</button>
                            </form>
                        </div>
                        <script>
                            function confirm_delete() {
                                var text = '{!! $img_upload->store !!}';
                                var confirm = window.confirm('ยืนยันการลบ' + text);
                                if (confirm) {
                                    document.getElementById('form-delete').submit();
                                }
                            }
                        </script>
                    </div>
                </div>
            </div>
                    @endforeach
                    @endif
        </div>
    </div>
@endsection
