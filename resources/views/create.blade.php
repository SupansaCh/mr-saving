@extends('layouts.app')

@section('content')
    <div class="container col-2 mt-3">
        <form method="post" action="{{url('store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>รูปภาพร้านอาหาร</label>
                <input type="file"  name="file" class="form-control" >
            </div>
            <div class="form-group">
                <label>ชื่อร้าน</label>
                <input type="text" name="store" class="form-control">
            </div>
            <div class="form-group">
                <label>ตำแหน่งที่ตั้งร้าน</label>
                <input type="text" name="place" class="form-control">
            </div>
            <div class="form-group">
                <label>เวลาเปิดร้าน</label>
                <input type="time" name="timeOpen" class="form-control">
            </div>
            <div class="form-group">
                <label>เวลาปิดร้าน</label>
                <input type="time" name="timeClose" class="form-control">
            </div>


            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>


@endsection
