
@extends('layouts.app')
@section('content')
    <div class="container">
        <form method="post" action="{{ url('uploads/'.$img_upload->id)  }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="store" class="form-control" value="{{ $img_upload->store }}">
            </div>
            <div class="form-group">
                <label>Content</label>
                <input type="text" name="place" class="form-control" value="{{ $img_upload->place }}">
            </div>
            <div class="form-group">
                <label>Content</label>
                <input type="text" name="TimeOpen" class="form-control" value="{{ $img_upload->timeOpen }}">
            </div>
            <div class="form-group">
                <label>Content</label>
                <input type="text" name="timeClose" class="form-control" value="{{ $img_upload->timeClose }}">
            </div>
            <button type="submit">แก้ไขเรียบร้อย</button>
        </form>
    </div>
@endsection
