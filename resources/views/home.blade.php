@extends('layouts.app')

@section('content')
    <!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
              integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <title>MR.SAVING</title>
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>

    <div class="container">
        <!-- Authentication Links -->
        <div class="row">
            <button type="button" class="col-6 btn btn-primary p-3" style="background-color: #C1DF5B; color: #222222; border: 0; border-radius: 0px;">คนเดียว</button>
            <button type="button" class="col-6 btn btn-secondary" style="background-color: #E6E7D3;color: #222222;border: 0; border-radius: 0px;">เป็นกลุ่ม</button>
        </div><!--row-->
        <label class="col-5 m-3" style="text-align:left;padding: 0;">ค่าอาหารที่ใช้ได้วันนี้</label>
        <label class="col-5" style="text-align:right;padding: 0;">150.00 ฿</label>
        <hr>



            <div class="row">
                <div class="col-sm">
                    สถานที่
                    <input type="text" aria-label="First name" class="form-control">
                </div>
                <div class="col-sm">
                    ชื่อเมนู
                    <div class="input-group mb-3">
                        <select class="custom-select" id="inputGroupSelect03" aria-label="Example select with button addon">
                            <option class="dropdown-item"  value="0">ข้าวหน้าเป็ด</option>
                            <option class="dropdown-item"  value="1">ข้าวไข่เจียว</option>
                            <option class="dropdown-item"  value="2">ข้าวกะเพราหมู</option>
                            <option class="dropdown-item"  value="3">ข้าวผัด</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm">
                    เรียงจาก
                    <div class="input-group mb-3">
                        <select class="custom-select" id="inputGroupSelect03" aria-label="Example select with button addon">
                            <option class="dropdown-item"  value="0">ราคาต่ำ-สูง</option>
                            <option class="dropdown-item"  value="1">ราคาสูง-ต่ำ</option>
                            <option class="dropdown-item"  value="2">ความนิยม</option>
                            <option class="dropdown-item"  value="3">ใกล้ฉัน</option>
                        </select>
                    </div>
                </div>
                <div class="container p-3">
                    <button type="button" class="btn btn-primary btn-lg btn-block" style="background-color: #C1DF5B; color: #222222; border: 0;">ค้นหาร้านค้า</button>
                    <p class="container p-2"></p>
                    <p class="p-2" style="background-color:#D6DBD0;">เมนูอาหาร</p>
                    <div class="row row-cols-2 row-cols-md-2">
                        <div class="col mb-4">
                            <div class="card mt-3" style="width: 90%;height: auto">
                                <img src="https://images.unsplash.com/photo-1584269600464-37b1b58a9fe7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2251&q=80" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">ข้าวผัดไข่</h5>
                                    <p class="card-text" style="color: #aaaaaa">ร้านแม่พิมพ์ใจ</p>
                                    <p class="card-text" style="color: #aaaaaa; font-size: small">4 กม.</p>
                                    <h4 class="card-title" style="color: yellowgreen;">60 ฿</h4>

                                    <a href="#" class="btn btn-outline-secondary btn-block" style=" border-radius: 20px;">เลือก</a>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-4">
                            <div class="card mt-3" style="width: 90%;height: auto">
                                <img src="https://images.unsplash.com/photo-1512058564366-18510be2db19?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2252&q=80" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h5 class="card-title">ข้าวผัดไข่</h5>
                                    <p class="card-text" style="color: #aaaaaa">ร้านเจ๊ปู</p>
                                    <p class="card-text" style="color: #aaaaaa; font-size: small">4 กม.</p>
                                    <h4 class="card-title" style="color: yellowgreen;">85 ฿</h4>

                                    <a href="#" class="btn btn-outline-secondary btn-block" style=" border-radius: 20px;">เลือก</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>






    </div><!--container info-->

    </body>

@endsection
