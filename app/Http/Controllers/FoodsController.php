<?php

namespace App\Http\Controllers;

use App\Foods;

use Illuminate\Http\Request;
class FoodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $img_uploads = Foods::OrderBy('created_at','desc')->get();
        return view('store')->with('img_uploads',$img_uploads);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return  view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
            [
                'file' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
                'store' => 'required',
                'place' => 'required',
                'timeOpen' => 'required',
                'timeClose' => 'required'

            ]);
        $img = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'),$img);

        $img_upload = new Foods();
        $img_upload->img = $img;
        $img_upload->store = $request->input('store');
        $img_upload->place = $request->input('place');
        $img_upload->timeOpen = $request->input('timeOpen');
        $img_upload->timeClose = $request->input('timeClose');
        $img_upload->save();

        return redirect('store')->with('success','Uploaded');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $img_upload = Foods::find($id);
        return view('store')->with('img_upload',$img_upload);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $img_upload = Foods::find($id);
        return view('edit')->with('img_upload',$img_upload);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $img_upload = Foods::find($id);
        $img_upload->store = $request->input('store');
        $img_upload->place = $request->input('place');
        $img_upload->timeOpen = $request->input('timeOpen');
        $img_upload->timeClose = $request->input('timeClose');
        $img_upload->save();
        return view('store')
            ->with('img_updated',$img_upload)
            ->with('editAgain',$img_upload->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $img_upload = Foods::find($id);
        $img_upload->delete();
        return redirect('/edit')->with('success','Delete Success'.'id = 000'.$img_upload->id);
    }
}
