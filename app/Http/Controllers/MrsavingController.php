<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Foods;

class MrsavingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,
            [
                'file' => 'required|mimes:jpeg,png,jpg,gif|max:2048',
                'store' => 'required',
                'place' => 'required',
                'timeOpen' => 'required',
                'timeClose' => 'required'

            ]);
        $img = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'),$img);

        $img_upload = new Foods();
        $img_upload->img = $img;
        $img_upload->store = $request->input('store');
        $img_upload->place = $request->input('place');
        $img_upload->timeOpen = $request->input('timeOpen');
        $img_upload->timeClose = $request->input('timeClose');
        $img_upload->save();

        return redirect('store')->with('success','Uploaded');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
